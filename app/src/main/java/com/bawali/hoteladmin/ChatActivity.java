package com.bawali.hoteladmin;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bawali.hoteladmin.adapter.ChatRoomThreadAdapter;
import com.bawali.hoteladmin.app.Config;
import com.bawali.hoteladmin.gcm.NotificationUtils;
import com.bawali.hoteladmin.utils.Database;
import com.bawali.hoteladmin.vo.Message;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "ChatActivity";

    private String contact_id;
    private String user_id;
    private RecyclerView recyclerView;
    private ChatRoomThreadAdapter mAdapter;
    private ArrayList<Message> messageArrayList;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private Database database;
    private WebsocketService websocket;

    private boolean mIsBound;
    @Bind(R.id.btn_send) Button _sendButton;
    @Bind(R.id.data) EditText _messageInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        database = new Database(this);

        Intent intent = getIntent();
        contact_id = intent.getStringExtra("contact_id");
        String title = intent.getStringExtra("name");

        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("user_id", "");

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (contact_id == null) {
            Toast.makeText(getApplicationContext(), "Contact Id not found!", Toast.LENGTH_SHORT).show();
            finish();
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        messageArrayList = new ArrayList<>();
        mAdapter = new ChatRoomThreadAdapter(this, messageArrayList, user_id);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push message is received
                    handlePushNotification(intent);
                }
            }
        };

        _sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        fetchChatThread();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        doBindService();
        NotificationUtils.clearNotifications();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        doUnbindService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     * */
    private void handlePushNotification(Intent intent) {
        Message message = (Message) intent.getSerializableExtra("message");
        if (message != null) {
            insertMessage(message);
            messageArrayList.add(message);
            mAdapter.notifyDataSetChanged();
            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
            }
        }
    }

    /**
     * Posting a new message in chat room
     * will send a message using websocket to our server. Our server forwards the message using xmpp
     * to all the devices as push notification
     * */
    private void sendMessage() {
        final String text = this._messageInput.getText().toString().trim();

        if (TextUtils.isEmpty(text)) {
            Toast.makeText(getApplicationContext(), "Enter a message", Toast.LENGTH_SHORT).show();
            return;
        }

        final String msgId = UUID.randomUUID().toString();;
        Message message = new Message();
        message.setContactId(contact_id);
        message.setOwner(user_id);
        message.setData(text);
        message.setId(msgId);
        message.setType("TEXT");
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //message.setCreatedAt(sdf.format(new Date()));
        message.setCreatedAt(new Date().getTime());
        messageArrayList.add(message);
        mAdapter.notifyDataSetChanged();

        websocket.sendMessage(message);
        insertMessage(message);

        recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);

        this._messageInput.getText().clear();


    }

    /**
     * Fetching all the messages of a single chat room
     * */
    private void fetchChatThread() {
        List<Message> chats = database.getChatHistory(contact_id);
        messageArrayList.addAll(chats);
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
        }
    }

    /**
     *
     * @param message
     */
    private void insertMessage(Message message) {
        long result = database.insertChat(message);
        Log.d(TAG, "after message update::"+result);
    }

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            WebsocketService.LocalBinder binder = (WebsocketService.LocalBinder) service;
            websocket = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
        }
    };

    private void doBindService() {
        if (!mIsBound) {
            Intent i = new Intent(getBaseContext(), WebsocketService.class);
            bindService(i, mServerConn, Context.BIND_AUTO_CREATE);
            mIsBound = true;
        }
    }

    private void doUnbindService() {
        if (mIsBound) {
            unbindService(mServerConn);
            mIsBound = false;
        }
    }

    private void registerReceiver(){
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        isReceiverRegistered = true;
    }
}

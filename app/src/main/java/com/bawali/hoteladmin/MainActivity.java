package com.bawali.hoteladmin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.bawali.hoteladmin.adapter.ContactListAdapter;
import com.bawali.hoteladmin.app.AppController;
import com.bawali.hoteladmin.app.Config;
import com.bawali.hoteladmin.utils.Database;
import com.bawali.hoteladmin.utils.WebService;
import com.bawali.hoteladmin.vo.Message;
import com.bawali.hoteladmin.vo.ProductContact;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Bind(R.id.list) ListView _listView;
    private ContactListAdapter adapter;
    private List<ProductContact> contactList;
    Database database;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checkPlayServicesAndUpdateToken();

        //Later get it from auth server
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("access_token",
                "xyz123").apply();

        database = new Database(this);
        getContactListWithMessage();


        adapter = new ContactListAdapter(this, contactList);
        adapter.notifyDataSetChanged();

        _listView.setAdapter(adapter);

        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                contactList.get(position).unreadCount = 0;
                adapter.notifyDataSetChanged();
                TextView contact_id = (TextView)view.findViewById(R.id.timestamp);
                TextView name = (TextView)view.findViewById(R.id.name);
                Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                intent.putExtra("contact_id",contact_id.getText());
                intent.putExtra("name",name.getText());
                startActivity(intent);
            }
        });


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    Log.d(TAG, "received a message");
                    handlePushNotification(intent);
                } else if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    SharedPreferences sharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sentToken = sharedPreferences
                            .getBoolean(Config.SENT_TOKEN_TO_SERVER, false);
                    if (sentToken) {

                    } else {

                    }
                }
            }
        };
        // Registering BroadcastReceiver
        registerReceiver();


        Intent i = new Intent(getBaseContext(), WebsocketService.class);
        startService(i); //to handle websocket

    }


    @Override
    public void onResume() {
        super.onResume();
        registerReceiver();
    }


    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }


    private void registerReceiver(){
        if(!isReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter(Config.REGISTRATION_COMPLETE);
            intentFilter.addAction(Config.PUSH_NOTIFICATION);
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    intentFilter);
            isReceiverRegistered = true;
        }
    }

    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     * */
    private void handlePushNotification(Intent intent) {
        Message message = (Message) intent.getSerializableExtra("message");
        if (message != null) {
            updateRow(message);
            database.insertChat(message);
        }
    }


    /**
     * Updates the chat list unread count and the last message
     */
    private void updateRow(Message message) {
        if(getContactById(message.getContactId())==null) {
            ProductContact c = new ProductContact();
            c.name = message.getContactId();
            c.contact_id = message.getContactId();
            c.lastMessage = message.getData();
            c.contact_type = "CUSTOMER";
            contactList.add(c);

            try {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("contact_id", c.contact_id);
                jsonObj.put("name", c.contact_id);
                jsonObj.put("contact_type", c.contact_type);
                database.insertProductContact(jsonObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (ProductContact contact : contactList) {
            if (contact.contact_id.equals(message.getContactId())) {
                int index = contactList.indexOf(contact);
                contact.lastMessage = message.getData();
                contact.unreadCount = contact.unreadCount + 1;
                contactList.remove(index);
                contactList.add(index, contact);
                break;
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void getContactListWithMessage(){
        contactList = database.getProductContacts();
        /*if(contactList.size()==0) {
            ProductContact defaultRow = new ProductContact();
            defaultRow.name = "No contact available";
            contactList.add(defaultRow);
        }*/
        String oldNotification = AppController.getInstance().getPrefManager().getNotifications();
        if(oldNotification!=null) {
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));
            for (int i = messages.size() - 1; i >= 0; i--) {
                String msg = messages.get(i);
                String contactId = msg.split("`")[0];
                ProductContact contact = getContactById(contactId);
                if (contact != null) {
                    contact.unreadCount += 1;
                }
            }
        }
    }

    private ProductContact getContactById(String contactId){
        for(ProductContact c: contactList) {
            if(contactId.equals(c.contact_id)){
                return c;
            }
        }
        return null;
    }


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServicesAndUpdateToken() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        new Timer().schedule(new TimerTask(){
            @Override
            public void run(){
                WebService.sendRegistrationToServer(MainActivity.this);
            }
        },10000);
        return true;
    }

}

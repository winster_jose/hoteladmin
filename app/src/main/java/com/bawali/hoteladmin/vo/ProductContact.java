package com.bawali.hoteladmin.vo;

import java.io.Serializable;

/**
 * Created by wjose on 05/07/2016.
 */
public class ProductContact implements Serializable {

    public String contact_id;
    public String name;
    public String contact_type;
    public String timestamp;
    public String lastMessage;
    public int unreadCount;

}

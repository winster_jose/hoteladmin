package com.bawali.hoteladmin.vo;

import java.io.Serializable;

/**
 * Created by wjose on 7/11/2016.
 */
public class Message implements Serializable {
    String id, data, contactId, owner;
    long createdAt;
    String type;

    public Message() {
    }

    public Message(String id, String data, String type, String contactId,
                   String owner, long createdAt) {
        this.id = id;
        this.data = data;
        this.type = type;
        this.contactId = contactId;
        this.owner = owner;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}

package com.bawali.hoteladmin.app;

/**
 * Created by wjose on 7/11/2016.
 */
public class Config {

    // flag to identify whether to show single line
    // or multi line text in push notification tray
    public static boolean appendNotificationMessages = false;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    //private static final String HTTP_DOMAIN = "http://10.0.2.2:8080";
    private static final String HTTP_DOMAIN = "https://stayahead.herokuapp.com";
    //private static final String WS_DOMAIN = "http://23.236.56.54:8080";
    private static final String WS_DOMAIN = "https://stayahead.herokuapp.com";

    public static final String GCM_TOKEN_URL = HTTP_DOMAIN+"/v1.0/token";
    public static final String OFFLINE_URL = HTTP_DOMAIN+"/v1.0/offline";
    public static final String CONNECTION_SOCKET_URL = HTTP_DOMAIN+"/v1.0/socket";
    public static final String WEBSOCKET_SERVER_URL = WS_DOMAIN;

    /**
     * The timeout value in milliseconds for socket connection.
     */
    public static final int WEBSOCKET_SERVER_TIMEOUT = 5000;


}
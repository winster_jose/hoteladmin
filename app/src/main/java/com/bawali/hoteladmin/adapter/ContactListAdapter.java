package com.bawali.hoteladmin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bawali.hoteladmin.R;
import com.bawali.hoteladmin.app.AppController;
import com.bawali.hoteladmin.vo.ProductContact;

import java.util.List;

/**
 * Created by wjose on 7/9/2016.
 */
public class ContactListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ProductContact> contactList;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ContactListAdapter(Activity activity, List<ProductContact> contactList) {
        this.activity = activity;
        this.contactList = contactList;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int location) {
        return contactList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView message = (TextView) convertView.findViewById(R.id.message);
        TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
        TextView count = (TextView) convertView.findViewById(R.id.count);

        // getting movie data for the row
        ProductContact m = contactList.get(position);

        name.setText(m.name);
        message.setText(m.lastMessage);
        timestamp.setText(m.contact_id);
        if(m.unreadCount>0) {
            count.setText(String.valueOf(m.unreadCount));
            count.setVisibility(View.VISIBLE);
        } else {
            count.setVisibility(View.GONE);
        }

        return convertView;
    }
}

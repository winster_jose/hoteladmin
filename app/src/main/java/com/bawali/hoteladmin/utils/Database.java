package com.bawali.hoteladmin.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bawali.hoteladmin.vo.Message;
import com.bawali.hoteladmin.vo.ProductContact;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wjose on 06/07/2016.
 */
public class Database  extends SQLiteOpenHelper {

    private static final String TAG = "Database";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "database.db";

    private static final String SQL_DELETE_MESSAGES = "DROP TABLE IF EXISTS messages";
    private static final String SQL_DELETE_PRODUCT_CONTACTS = "DROP TABLE IF EXISTS product_contacts";
    public static final String SQL_TRUNCATE_MESSAGES = "DELETE FROM messages";
    private static final String SQL_TRUNCATE_PRODUCT_CONTACTS = "DELETE FROM product_contacts";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE product_contacts (contact_id TEXT, " +
                        "name TEXT, contact_type TEXT, PRIMARY KEY (contact_id))"
        );
        db.execSQL(
                "CREATE TABLE messages (message_id TEXT, contact_id TEXT, " +
                        "data TEXT, type TEXT, owner TEXT, created_at INTEGER, " +
                        "PRIMARY KEY (message_id, contact_id))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        clearDatabase(db);
        onCreate(db);
    }

    public void clearDatabase(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_PRODUCT_CONTACTS);
        db.execSQL(SQL_DELETE_MESSAGES);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }


    public long insertProductContact(JSONObject contact) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("contact_id", contact.getString("contact_id"));
        contentValues.put("name", contact.getString("name"));
        contentValues.put("contact_type", contact.getString("contact_type"));
        return db.insert("product_contacts", null, contentValues);
    }

    public List<ProductContact> getProductContacts() {
        List<ProductContact> productContacts = new ArrayList<ProductContact>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select contact_id, name, contact_type from product_contacts",
                new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {
            ProductContact contact = new ProductContact();
            contact.contact_id=res.getString(res.getColumnIndex("contact_id"));
            contact.name=res.getString(res.getColumnIndex("name"));
            contact.contact_type=res.getString(res.getColumnIndex("contact_type"));
            productContacts.add(contact);
            res.moveToNext();
        }
        return productContacts;
    }

    public ProductContact getProductContact(String contactId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select contact_id, name, contact_type from product_contacts" +
                " where contact_id=?", new String[]{contactId});
        res.moveToFirst();
        ProductContact contact = new ProductContact();
        contact.contact_id=res.getString(res.getColumnIndex("contact_id"));
        contact.name=res.getString(res.getColumnIndex("name"));
        contact.contact_type=res.getString(res.getColumnIndex("contact_type"));
        return contact;
    }

    public long updateProductContact(String contact_id, String name, String contact_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("contact_type", contact_type);
        return db.update("product_contacts", contentValues, "contact_id=?",
                new String[]{String.valueOf(contact_id)});
    }

    public long deleteProductContact(String contact_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("product_contacts", "contact_id=?",
                new String[]{String.valueOf(contact_id)});
    }

    public long insertChat(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("owner", message.getOwner());
        contentValues.put("message_id", message.getId());
        contentValues.put("contact_id", message.getContactId());
        contentValues.put("data", message.getData());
        contentValues.put("type", message.getType());
        contentValues.put("created_at", message.getCreatedAt());
        Log.d(TAG, "insert chat:: "+contentValues.toString());
        return db.insert("messages", null, contentValues);
    }

    public long deleteChat(Message message) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("messages", "message_id=?",
                new String[]{message.getId()});
    }

    public List<Message> getChatHistory(String contact_id) {
        List<Message> messages = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select message_id, contact_id, owner, data, type, created_at from messages" +
                        " where contact_id = ? order by created_at",
                new String[]{contact_id});
        res.moveToFirst();
        Message message = null;
        while (!res.isAfterLast()) {
            message = new Message();
            message.setContactId(res.getString(res.getColumnIndex("contact_id")));
            message.setOwner(res.getString(res.getColumnIndex("owner")));
            message.setData(res.getString(res.getColumnIndex("data")));
            message.setId(res.getString(res.getColumnIndex("message_id")));
            message.setType(res.getString(res.getColumnIndex("type")));
            message.setCreatedAt(res.getLong(res.getColumnIndex("created_at")));
            messages.add(message);
            res.moveToNext();
        }
        return messages;
    }

    public void execScript(String script) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(script);
    }

}

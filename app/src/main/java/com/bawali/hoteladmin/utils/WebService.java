package com.bawali.hoteladmin.utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bawali.hoteladmin.app.AppController;
import com.bawali.hoteladmin.app.Config;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wjose on 05/07/2016.
 */
public class WebService {

    private static final String TAG = "Webservice";

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param context
     */
    public static void sendRegistrationToServer(final Context context) {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + token);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("token", token);
            JsonObjectRequest gmcTokenReq = new JsonObjectRequest(Request.Method.POST, Config.GCM_TOKEN_URL, jsonRequest,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                }
            }
            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    //add params <key,value>
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> headers = new HashMap<>();
                    String user_id = PreferenceManager.getDefaultSharedPreferences(context).getString("hotel", "");
                    String access_token = PreferenceManager.getDefaultSharedPreferences(context).getString("access_token", "");
                    String credentials = String.format("%s:%s",user_id,access_token);
                    String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(gmcTokenReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void updateSocketId(final Context context, String connectId) throws JSONException {
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("connection_id", connectId);
        JsonObjectRequest updateSocketReq = new JsonObjectRequest(Request.Method.POST, Config.CONNECTION_SOCKET_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Socket response::" + response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //add params <key,value>
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                String user_id = PreferenceManager.getDefaultSharedPreferences(context).getString("hotel", "");
                String access_token = PreferenceManager.getDefaultSharedPreferences(context).getString("access_token", "");
                String credentials = String.format("%s:%s", user_id, access_token);
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(updateSocketReq);
    }

    public static void offline(final Context context) {
        JSONObject jsonRequest = new JSONObject();
        JsonObjectRequest offlineReq = new JsonObjectRequest(Request.Method.POST, Config.OFFLINE_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Offline response::"+response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //add params <key,value>
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();
                String user_id = PreferenceManager.getDefaultSharedPreferences(context).getString("user_id", "");
                String access_token = PreferenceManager.getDefaultSharedPreferences(context).getString("access_token", "");
                String credentials = String.format("%s:%s",user_id,access_token);
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(offlineReq);
    }

}
